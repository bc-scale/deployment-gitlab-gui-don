locals {
  tags = []
  default_labels = {
    "critical bug::critical" : {
      description = "Bug that impairs the main features of the tool; Must be fixes as soon as possible"
      color       = "#FF0000"
    },
    "bug::high" : {
      description = "Generic bug in the main features of the tool, with a known/standard way to fix it"
      color       = "#FF0000"
    },
    "edge bug::medium" : {
      description = "Bug on a side; not frequently used feature"
      color       = "#c39953"
    },
    "heisenbug::medium" : {
      description = "Bug that seems to disapear in an attempt to study it"
      color       = "#c39953"
    },
    "chore" : {
      description = "Issue that requires some basic maintenance of the code: pins new version, dependency updates, etc"
      color       = "#5843AD"
    },
    "doc" : {
      description = "Issue that requires editing or adding new documentation"
      color       = "#5843AD"
    },
    "enhancement" : {
      description = "Issue that ask for a new feature"
      color       = "#5843AD"
    },
    "need more info" : {
      description = "Issue that require the input of a user before taking any action"
      color       = "#6699cc"
    },
    "to describe" : {
      description = "Issue not sufficiently described. All the necessary information is known, but the issue itself lacks written information."
      color       = "#6699cc"
    },
    "to sort" : {
      description = "Issue that need some labeling."
      color       = "#6699cc"
    }
  }
}

module "group" {
  source = "./modules/group"

  for_each = local.groups

  group_id      = lookup(each.value, "group_id", null)
  access_tokens = lookup(each.value, "access_tokens", null)

  auto_devops_enabled               = lookup(each.value, "auto_devops_enabled", false)
  default_branch_protection         = lookup(each.value, "default_branch_protection", 3)
  description                       = lookup(each.value, "description", null)
  emails_disabled                   = lookup(each.value, "emails_disabled", false)
  group_memberships                 = lookup(each.value, "group_memberships", {})
  lfs_enabled                       = lookup(each.value, "lfs_enabled", false)
  mentions_disabled                 = lookup(each.value, "mentions_disabled ", false)
  name                              = each.key
  parent_id                         = lookup(each.value, "parent_id", 0)
  path                              = lookup(each.value, "path", null)
  project_creation_level            = lookup(each.value, "project_creation_level", "maintainer")
  request_access_enabled            = lookup(each.value, "request_access_enabled", false)
  require_two_factor_authentication = lookup(each.value, "require_two_factor_authentication", true)
  subgroup_creation_level           = lookup(each.value, "subgroup_creation_level", "owner")
  two_factor_grace_period           = lookup(each.value, "two_factor_grace_period", 336)
  visibility_level                  = lookup(each.value, "visibility_level", "public")
  labels                            = lookup(each.value, "labels", {})
}

module "project" {
  source = "./modules/project"

  for_each = merge(
    local.terraform_projects,
    local.bash_projects,
    local.container_projects,
  )

  group_id = lookup(each.value, "group_id")

  name = each.key

  analytics_access_level                = lookup(each.value, "analytics_access_level", "private")
  container_registry_enabled            = lookup(each.value, "container_registry_enabled", false)
  default_branch_push_access_level      = lookup(each.value, "default_branch_push_access_level", "no one")
  description                           = lookup(each.value, "description")
  import_url                            = lookup(each.value, "import_url", null)
  labels                                = lookup(each.value, "labels", {})
  only_allow_merge_if_pipeline_succeeds = lookup(each.value, "only_allow_merge_if_pipeline_succeeds", true)
  pages_access_level                    = lookup(each.value, "pages_access_level", "disabled")
  tags                                  = lookup(each.value, "tags", local.tags)
  variables                             = lookup(each.value, "variables", {})

  branch_protection_allowed_to_push = [{ user_id = data.gitlab_user.owner.id }]
}
