locals {
  bash_projects = {
    rpi-coreos-installer : {
      description = "Raspberry Pi Coreos Installer"
      tags        = concat(local.bash_tags, ["gitlab"]),
      group_id    = module.group["bash"].id
      import_url  = "https://gitlab.com/gui-don/bash-rpi-coreos-installer.git"
    }
  }
  bash_tags = ["bash"]
}
