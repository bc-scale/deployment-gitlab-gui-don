locals {
  container_projects = {
    container-php-cicd : {
      description = "Container for PHP CI/CD"
      tags        = concat(local.container_tags, ["php"]),
      group_id    = module.group["container"].id
      import_url  = "https://gitlab.com/gui-don/moustache-ci-docker.git"

      container_registry_enabled = true
    }
  }
  container_tags = ["oci", "container"]
}
