locals {
  terraform_projects = {
    deployment-gitlab-gui-don : {
      description                      = "Deployment project to setup gitlab repositories, settings and groups."
      tags                             = concat(local.terraform_deployment_tags, ["gitlab"]),
      default_branch_push_access_level = "maintainer"
      group_id                         = module.group["terraform"].id
      variables = {
        TF_gitlab_token = {
          value     = module.group["wanyuu"].access_tokens["RootAccess"].token
          protected = true
          masked    = true
        }
      }
    }
  }
  terraform_tags            = ["terraform"]
  terraform_deployment_tags = concat(local.terraform_tags, ["deployment"])
  #  terraform_module_tags     = concat(local.terraform_tags, ["module"])
}
