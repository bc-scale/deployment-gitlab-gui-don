#####
# Group
#####

locals {
  group_id = var.group_id == null ? gitlab_group.this[0].id : var.group_id
}

resource "gitlab_group" "this" {
  for_each = var.group_id == null ? { 0 = "enabled" } : {}

  auto_devops_enabled               = var.auto_devops_enabled
  default_branch_protection         = var.default_branch_protection
  description                       = var.description
  emails_disabled                   = var.emails_disabled
  lfs_enabled                       = var.lfs_enabled
  mentions_disabled                 = var.mentions_disabled
  name                              = var.name
  parent_id                         = var.parent_id
  path                              = var.path
  project_creation_level            = var.project_creation_level
  request_access_enabled            = var.request_access_enabled
  require_two_factor_authentication = var.require_two_factor_authentication
  share_with_group_lock             = var.share_with_group_lock
  subgroup_creation_level           = var.subgroup_creation_level
  two_factor_grace_period           = var.two_factor_grace_period
  visibility_level                  = var.visibility_level

  lifecycle {
    prevent_destroy = true
  }
}

#####
# Membership
#####

resource "gitlab_group_membership" "this" {
  for_each = var.group_memberships

  group_id     = local.group_id
  user_id      = lookup(each.value, "user_id")
  access_level = lookup(each.value, "access_level", "maintainer")
  expires_at   = lookup(each.value, "expires_at", null)
}

#####
# Labels
#####

resource "gitlab_group_label" "this" {
  for_each = var.labels

  group = local.group_id

  name        = each.key
  description = lookup(each.value, "description")
  color       = lookup(each.value, "color")
}

#####
# Access Token
#####

resource "gitlab_group_access_token" "this" {
  for_each = var.access_tokens == null ? {} : var.access_tokens

  group = local.group_id

  name   = each.key
  scopes = lookup(each.value, "scopes")

  access_level = lookup(each.value, "access_level", null)
  expires_at   = lookup(each.value, "expires_at", null)
}
