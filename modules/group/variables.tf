variable "group_id" {
  type    = number
  default = null
}

variable "name" {
  type = string
}

variable "path" {
  type = string
}

variable "description" {
  type = string
}

variable "lfs_enabled" {
  type    = bool
  default = false
}

variable "default_branch_protection" {
  type = number
}

variable "request_access_enabled" {
  type    = bool
  default = false
}

variable "visibility_level" {
  type    = string
  default = "public"
}

variable "share_with_group_lock" {
  type    = bool
  default = true
}

variable "project_creation_level" {
  type    = string
  default = "noone"
}

variable "auto_devops_enabled" {
  type    = bool
  default = false
}

variable "emails_disabled" {
  type    = bool
  default = false
}

variable "mentions_disabled" {
  type    = bool
  default = false
}

variable "subgroup_creation_level" {
  type    = string
  default = "owner"
}

variable "require_two_factor_authentication" {
  type    = bool
  default = false
}

variable "two_factor_grace_period" {
  type    = number
  default = null
}

variable "parent_id" {
  type    = number
  default = null
}

variable "group_memberships" {
  type = map(object({
    user_id      = number
    access_level = optional(string)
    expires_at   = optional(string)
  }))
}

variable "labels" {
  type = map(object({
    description = string
    color       = string
  }))
}

#####
# Access Token
#####

variable "access_tokens" {
  description = <<EOF
    Key: name: The name of the group access token.

    scopes (set(string), required)  The scope for the group access token. It determines the actions which can be performed when authenticating with this token. Valid values are: api, read_api, read_registry, write_registry, read_repository, write_repository.
    access_level (string, optional) The access level for the group access token. Valid values are: guest, reporter, developer, maintainer, owner.
    expires_at (string, optional)   The token expires at midnight UTC on that date. The date must be in the format YYYY-MM-DD. Default is never.
EOF
  type = map(object({
    scopes       = set(string)
    access_level = optional(string)
    expires_at   = optional(string)
  }))
  default = null
}
