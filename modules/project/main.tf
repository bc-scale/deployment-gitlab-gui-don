#####
# Projects
#####

locals {
  default_branch = "master"

  default_container_registry_enabled = var.container_registry_enabled
  default_issues_enabled             = true
  default_lfs_enabled                = false
  default_merge_requests_enabled     = true
  default_packages_enabled           = false
  default_pipelines_enabled          = true
  default_request_access_enabled     = true
  default_snippets_enabled           = false
  default_wiki_enabled               = false

  default_merge_method                                     = "ff"
  default_only_allow_merge_if_all_discussions_are_resolved = true
  default_resolve_outdated_diff_discussions                = true
  default_only_allow_merge_if_pipeline_succeeds            = true
  default_security_and_compliance_access_level             = "private"
  default_approvals_before_merge                           = 2

  // All commit messages must match this regex
  commit_message_regex = "(feat|fix|chore|tech|refactor|doc|maintenance|test|revert): "
  // All commited filenames must not match this regex
  file_name_regex = "(exe|tar|tar.gz|tar.xz|zip|7zip|rar|bin|jar|docx|xlsx|xls)$"
  // Users can only push commits to this repository that were committed with one of their own verified emails.
  commit_committer_check = true
  deny_delete_tag        = true
  // GitLab will reject any files that are likely to contain secrets.
  prevent_secrets         = true
  reject_unsigned_commits = true
  // Maximum file size (MB).
  max_file_size = 2


}

resource "gitlab_project" "this" {
  name        = var.name
  description = var.description
  tags        = var.tags

  namespace_id                     = var.group_id
  default_branch                   = local.default_branch
  visibility_level                 = "public"
  initialize_with_readme           = true
  remove_source_branch_after_merge = true
  allow_merge_on_skipped_pipeline  = false

  analytics_access_level     = var.analytics_access_level
  container_registry_enabled = local.default_container_registry_enabled
  issues_enabled             = local.default_issues_enabled
  lfs_enabled                = local.default_lfs_enabled
  merge_requests_enabled     = local.default_merge_requests_enabled
  packages_enabled           = local.default_packages_enabled
  pages_access_level         = var.pages_access_level
  pipelines_enabled          = local.default_pipelines_enabled
  request_access_enabled     = local.default_request_access_enabled
  shared_runners_enabled     = var.shared_runners_enabled
  snippets_enabled           = local.default_snippets_enabled
  wiki_enabled               = local.default_wiki_enabled

  merge_method                                     = local.default_merge_method
  only_allow_merge_if_pipeline_succeeds            = coalesce(var.only_allow_merge_if_pipeline_succeeds, local.default_only_allow_merge_if_pipeline_succeeds)
  approvals_before_merge                           = local.default_approvals_before_merge
  only_allow_merge_if_all_discussions_are_resolved = local.default_only_allow_merge_if_all_discussions_are_resolved
  security_and_compliance_access_level             = local.default_security_and_compliance_access_level
  resolve_outdated_diff_discussions                = local.default_resolve_outdated_diff_discussions

  push_rules {
    commit_message_regex    = local.commit_message_regex
    file_name_regex         = local.file_name_regex
    commit_committer_check  = local.commit_committer_check
    deny_delete_tag         = local.deny_delete_tag
    prevent_secrets         = local.prevent_secrets
    reject_unsigned_commits = local.reject_unsigned_commits
    max_file_size           = local.max_file_size
  }

  import_url = var.import_url

  lifecycle {
    prevent_destroy = true
  }
}

resource "gitlab_project_level_mr_approvals" "this" {
  project_id = gitlab_project.this.id

  disable_overriding_approvers_per_merge_request = true
  merge_requests_author_approval                 = true
  merge_requests_disable_committers_approval     = false
  require_password_to_approve                    = false
  reset_approvals_on_push                        = false
}

resource "gitlab_label" "this" {
  for_each = var.labels

  project = gitlab_project.this.id

  name        = each.key
  description = lookup(each.value, "description")
  color       = lookup(each.value, "color")
}

resource "gitlab_project_badge" "this" {
  project   = gitlab_project.this.id
  link_url  = "https://gitlab.com/%%{project_path}/-/commits/%%{default_branch}"
  image_url = "https://gitlab.com/%%{project_path}/badges/%%{default_branch}/pipeline.svg"
}

#####
# Branch Protection
#####

resource "gitlab_branch_protection" "this" {
  for_each = toset(compact(concat([local.default_branch], var.branch_protection_branches)))

  project = gitlab_project.this.id

  branch                       = each.value
  push_access_level            = var.branch_protection_push_access_level
  merge_access_level           = var.branch_protection_merge_access_level
  allow_force_push             = var.branch_protection_allow_force_push
  code_owner_approval_required = var.branch_protection_code_owner_approval_required

  dynamic "allowed_to_push" {
    for_each = var.branch_protection_allowed_to_push

    content {
      user_id = lookup(allowed_to_push.value, "user_id")
    }
  }

  dynamic "allowed_to_merge" {
    for_each = var.branch_protection_allowed_to_merge

    content {
      user_id = lookup(allowed_to_merge.value, "user_id")
    }
  }
}

#####
# Variables
#####

resource "gitlab_project_variable" "this" {
  for_each = var.variables

  project = gitlab_project.this.id

  key           = each.key
  value         = lookup(each.value, "value")
  protected     = lookup(each.value, "protected", null)
  masked        = lookup(each.value, "masked", null)
  variable_type = lookup(each.value, "variable_type", null)
}
