provider "gitlab" {
  token    = var.gitlab_token
  base_url = ""
}

terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/40888819/terraform/state/production"
    lock_address   = "https://gitlab.com/api/v4/projects/40888819/terraform/state/production/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/40888819/terraform/state/production/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}
