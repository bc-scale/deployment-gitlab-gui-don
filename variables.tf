#####
# Global
#####

variable "gitlab_token" {
  type        = string
  description = "gitlab application token. [See how to create a personal access token on gitlab](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html). "
  sensitive   = true
}
